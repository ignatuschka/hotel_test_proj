import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';

import 'package:hotel_test_project/core/dio_module.dart';
import 'package:hotel_test_project/features/hotel_module/data/data_source/hotel_remote_data_source.dart';
import 'package:hotel_test_project/features/hotel_module/data/network/hotel_api.dart';
import 'package:hotel_test_project/features/hotel_module/data/repository_impl/hotel_repository.dart';
import 'package:hotel_test_project/features/hotel_module/domain/repository/hotel_repository.dart';
import 'package:hotel_test_project/features/hotel_module/domain/usecase/get_booking_data_usecase.dart';
import 'package:hotel_test_project/features/hotel_module/domain/usecase/get_hotel_data_usecase.dart';
import 'package:hotel_test_project/features/hotel_module/domain/usecase/get_hotel_room_usecase.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/booking_screen/bloc/booking_screen_bloc.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/booking_screen/booking_screen.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/hotel_room_screen/bloc/hotel_room_screen_bloc.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/hotel_room_screen/hotel_room_screen.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/hotel_screen/bloc/hotel_screen_bloc.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/hotel_screen/hotel_screen.dart';

class HotelDIModule {
  void updateInjections(GetIt instance) {
    instance.registerLazySingleton<HotelRemoteDataSource>(
      () => HotelRemoteDataSourceImpl(
        hotelApi: instance.get(),
      ),
    );

    instance.registerSingleton<Dio>(DioModule.provideDio());

    instance.registerFactory(() => HotelApi(instance.get()));

    instance.registerLazySingleton<HotelRepository>(() => HotelRepositoryImpl(remoteDataSource: instance.get()));

    instance.registerFactory(() => GetBookingDataUsecase(repository: instance.get()));

    instance.registerFactory(() => GetHotelDataUsecase(repository: instance.get()));

    instance.registerFactory(() => GetHotelRoomsUsecase(repository: instance.get()));
  }
}

class BookingPage extends StatelessWidget {
  const BookingPage({super.key});

  GetIt get getIt => GetIt.I;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BookingScreenBloc(
        getBookingDataUsecase: getIt(),
        navigator: getIt(),
      ),
      child: const BookingScreen(),
    );
  }
}

class HotelRoomPage extends StatelessWidget {
  final String hotelName;
  const HotelRoomPage({
    Key? key,
    required this.hotelName,
  }) : super(key: key);

  GetIt get getIt => GetIt.I;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HotelRoomScreenBloc(
        getHotelRoomsUsecase: getIt(),
        navigator: getIt(),
      ),
      child: HotelRoomsScreen(hotelName: hotelName),
    );
  }
}

class HotelPage extends StatelessWidget {
  const HotelPage({super.key});

  GetIt get getIt => GetIt.I;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HotelScreenBloc(
        getHotelDataUsecase: getIt(),
        navigator: getIt(),
      ),
      child: const HotelScreen(),
    );
  }
}
