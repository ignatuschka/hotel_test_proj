import 'package:flutter/material.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/display_card.dart';
import 'package:hotel_test_project/resources/core_text.dart';
import 'package:hotel_test_project/widgets/body_medium_text.dart';

class BookingInfo extends StatelessWidget {
  final String departure;
  final String arrivalCountry;
  final String dates;
  final String numberOfNights;
  final String hotelName;
  final String room;
  final String nutrition;
  const BookingInfo({
    Key? key,
    required this.departure,
    required this.arrivalCountry,
    required this.dates,
    required this.numberOfNights,
    required this.hotelName,
    required this.room,
    required this.nutrition,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DisplayCard(
      content: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width / 2 - 16,
                  child: const BodyMediumText(
                    text: CoreText.flightFrom,
                    color: AppPallete.greyText,
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width / 2 - 16,
                  child: BodyMediumText(
                    text: departure,
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2 - 16,
                    child: const BodyMediumText(
                      text: CoreText.countryCity,
                      color: AppPallete.greyText,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2 - 16,
                    child: BodyMediumText(
                      text: arrivalCountry
                    ),
                  ),
                ],
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width / 2 - 16,
                  child: const BodyMediumText(
                    text: CoreText.dates,
                    color: AppPallete.greyText,
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width / 2 - 16,
                  child:
                      BodyMediumText(text: dates),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2 - 16,
                    child: const BodyMediumText(
                      text: CoreText.nightCount,
                      color: AppPallete.greyText,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2 - 16,
                    child: BodyMediumText(
                      text: numberOfNights,
                    ),
                  ),
                ],
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width / 2 - 16,
                  child: const BodyMediumText(
                    text: CoreText.hotel,
                    color: AppPallete.greyText,
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width / 2 - 16,
                  child: BodyMediumText(
                    text: hotelName,
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2 - 16,
                    child: const BodyMediumText(
                      text: CoreText.room,
                      color: AppPallete.greyText,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2 - 16,
                    child: BodyMediumText(
                      text: room,
                    ),
                  ),
                ],
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width / 2 - 16,
                  child: const BodyMediumText(
                    text: CoreText.meals,
                    color: AppPallete.greyText,
                  ),
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width / 2 - 16,
                  child: BodyMediumText(
                    text: nutrition,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
