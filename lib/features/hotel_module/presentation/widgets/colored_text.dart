import 'package:flutter/material.dart';
import 'package:hotel_test_project/widgets/body_medium_text.dart';

class ColoredWidget extends StatelessWidget {
  final String? text;
  final Color widgetsColor;
  final Color backgroundColor;
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final EdgeInsetsGeometry? padding;
  const ColoredWidget({
    Key? key,
    this.text,
    required this.widgetsColor,
    required this.backgroundColor,
    this.prefixIcon,
    this.suffixIcon,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Padding(
        padding: padding ?? const EdgeInsets.fromLTRB(10, 5, 10, 5),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            if (prefixIcon != null) ...[
              Icon(prefixIcon, color: widgetsColor),
              const SizedBox(width: 2),
            ],
            if (text != null)
              BodyMediumText(
                text: text ?? '',
                color: widgetsColor,
              ),
            if (suffixIcon != null) ...[
              if (text != null) const SizedBox(width: 2),
              Icon(suffixIcon, color: widgetsColor),
            ],
          ],
        ),
      ),
    );
  }
}
