import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:hotel_test_project/core/theme/app_pallete.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/dots_indicator.dart';
import 'package:hotel_test_project/widgets/custom_loading_indicator.dart';

class ImageCarousel extends StatelessWidget {
  final int pagesCount;
  final int currentIndex;
  final List<String> urls;
  final ValueChanged<int>? onPageChanged;
  const ImageCarousel({
    Key? key,
    required this.pagesCount,
    required this.currentIndex,
    required this.urls,
    this.onPageChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        SizedBox(
          height: 257,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(15),
            child: PageView.builder(
              itemCount: pagesCount,
              pageSnapping: true,
              itemBuilder: (BuildContext context, int index) {
                return CachedNetworkImage(
                  imageUrl: urls[index],
                  placeholder: (context, url) => const CustomLoadingIndicator(
                    color: AppPallete.blue,
                  ),
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                  fit: BoxFit.fill,
                );
              },
              onPageChanged: onPageChanged,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: DotsIndicator(
            dotRadius: 3.5,
            dotCount: pagesCount,
            currentIndex: currentIndex,
          ),
        ),
      ],
    );
  }
}
