import 'package:flutter/material.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';

class DotsIndicator extends StatelessWidget {
  final double dotRadius;
  final int dotCount;
  final int currentIndex;
  const DotsIndicator({
    Key? key,
    required this.dotRadius,
    required this.dotCount,
    required this.currentIndex,
  }) : super(key: key);

  Color getColor({required int index, required int currentIndex}) {
    switch ((index - currentIndex).abs()) {
      case 0:
        return AppPallete.black;
      case 1:
        return AppPallete.black.withOpacity(0.22);
      case 2:
        return AppPallete.black.withOpacity(0.17);
      case 3:
        return AppPallete.black.withOpacity(0.1);
      case 4:
        return AppPallete.black.withOpacity(0.05);
      default:
        return AppPallete.black;
    }
  }

  @override
  Widget build(BuildContext context) {
    final dots = List<Widget>.generate(
        dotCount,
        (index) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2.5),
              child: Container(
                height: dotRadius * 2,
                width: dotRadius * 2,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: getColor(
                    currentIndex: currentIndex,
                    index: index,
                  ),
                ),
              ),
            ));
    return Container(
      decoration: BoxDecoration(color: AppPallete.white, borderRadius: BorderRadius.circular(5)),
      child: Padding(
          padding: const EdgeInsets.fromLTRB(7.5, 5, 7.5, 5),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: dots,
          )),
    );
  }
}
