import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';
import 'package:hotel_test_project/widgets/body_medium_text.dart';
import 'package:hotel_test_project/widgets/body_small_text.dart';

class HotelDataButtons extends StatelessWidget {
  final List<String> assetPaths;
  final List<String> titles;
  final List<String> subtitles;
  final List<IconData> icons;
  const HotelDataButtons({
    Key? key,
    required this.assetPaths,
    required this.titles,
    required this.subtitles,
    required this.icons,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppPallete.greyBackground,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: List<Widget>.generate(
              titles.length,
              (index) => Column(
                    children: [
                      Row(
                        children: [
                          SvgPicture.asset(assetPaths[index]),
                          const SizedBox(width: 12),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              BodyMediumText(text: titles[index]),
                              const SizedBox(height: 2),
                              BodySmallText(text: subtitles[index], color: AppPallete.greyText),
                            ],
                          ),
                          const Spacer(),
                          Icon(icons[index]),
                        ],
                      ),
                      if (index < titles.length - 1)
                        const Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: Divider(),
                        ),
                    ],
                  )),
        ),
      ),
    );
  }
}
