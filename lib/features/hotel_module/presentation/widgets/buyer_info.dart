import 'package:flutter/material.dart';

import 'package:hotel_test_project/core/theme/app_pallete.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/custom_mask.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/display_card.dart';
import 'package:hotel_test_project/resources/core_text.dart';
import 'package:hotel_test_project/widgets/body_large_text.dart';
import 'package:hotel_test_project/widgets/body_small_text.dart';
import 'package:hotel_test_project/widgets/custom_textfield.dart';

class BuyerInfo extends StatefulWidget {
  final String? customMaskError;
  final String? emailError;
  final TextEditingController emailController;
  final void Function(String)? onChanged;
  final CustomMask customMask;
  const BuyerInfo({
    Key? key,
    this.customMaskError,
    this.emailError,
    this.onChanged,
    required this.emailController,
    required this.customMask,
  }) : super(key: key);

  @override
  State<BuyerInfo> createState() => _BuyerInfoState();
}

class _BuyerInfoState extends State<BuyerInfo> {
  @override
  Widget build(BuildContext context) {
    return DisplayCard(
      content: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const BodyLargeText(text: CoreText.buyerInfo),
            Padding(
              padding: const EdgeInsets.only(top: 8),
              child: CustomTextField(
                hintText: CoreText.telNumber,
                inputFormatters: [widget.customMask],
                onChanged: widget.onChanged,
                errorText: widget.customMaskError,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8),
              child: CustomTextField(
                hintText: CoreText.email,
                controller: widget.emailController,
                errorText: widget.emailError,
              ),
            ),
            const BodySmallText(
              text: CoreText.infoDescription,
              color: AppPallete.greyText,
            )
          ],
        ),
      ),
    );
  }
}
