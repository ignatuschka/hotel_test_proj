import 'package:flutter/material.dart';
import 'package:hotel_test_project/core/globals.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/colored_text.dart';

import 'package:hotel_test_project/features/hotel_module/presentation/widgets/display_card.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/image_carousel.dart';
import 'package:hotel_test_project/resources/core_text.dart';
import 'package:hotel_test_project/widgets/body_large_text.dart';
import 'package:hotel_test_project/widgets/body_medium_secondary_text.dart';
import 'package:hotel_test_project/widgets/custom_button.dart';
import 'package:hotel_test_project/widgets/display_large_text.dart';

class RoomCard extends StatefulWidget {
  final List<String> roomImageUrls;
  final String roomText;
  final List<String> aboutRoom;
  final int roomCost;
  final String costForWhat;
  final void Function() onTap;
  const RoomCard({
    Key? key,
    required this.roomImageUrls,
    required this.roomText,
    required this.aboutRoom,
    required this.roomCost,
    required this.onTap,
    required this.costForWhat,
  }) : super(key: key);

  @override
  State<RoomCard> createState() => _RoomCardState();
}

class _RoomCardState extends State<RoomCard> {
  int currentIndex = 0;

  void onPageChanged(int value) {
    setState(() {
      currentIndex = value;
    });
  }

  @override
  void initState() {
    currentIndex  = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DisplayCard(
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 16, bottom: 8),
            child: ImageCarousel(
              pagesCount: widget.roomImageUrls.length,
              currentIndex: currentIndex,
              urls: widget.roomImageUrls,
              onPageChanged:(value) {
                onPageChanged(value);
              },
            ),
          ),
          BodyLargeText(text: widget.roomText),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 4),
            child: Wrap(
              children: List<Widget>.generate(
                widget.aboutRoom.length,
                (index) => Padding(
                  padding: const EdgeInsets.all(4),
                  child: ColoredWidget(
                    text: widget.aboutRoom[index],
                    widgetsColor: AppPallete.greyText,
                    backgroundColor: AppPallete.greyTextBackground,
                  ),
                ),
              ),
            ),
          ),
          const ColoredWidget(
            text: CoreText.moreRoomDerailes,
            widgetsColor: AppPallete.blue,
            backgroundColor: AppPallete.blueOpacity,
            suffixIcon: Icons.keyboard_arrow_right,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                DisplayLargeText(
                  text: '${formatter.format(widget.roomCost).replaceAll(',', ' ')} \u20BD',
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8, bottom: 3),
                  child: BodyMediumSecondaryText(
                    text: widget.costForWhat.toLowerCase(),
                    color: AppPallete.greyText,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 16),
            child: CustomButton(
              onTap: widget.onTap,
              text: CoreText.toRoom,
            ),
          ),
        ],
      ),
    );
  }
}
