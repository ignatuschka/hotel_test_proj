import 'package:flutter/material.dart';

import 'package:hotel_test_project/core/theme/app_pallete.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/colored_text.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/display_card.dart';
import 'package:hotel_test_project/resources/core_text.dart';
import 'package:hotel_test_project/widgets/body_large_text.dart';
import 'package:hotel_test_project/widgets/custom_textfield.dart';

class AddTouristInfo extends StatefulWidget {
  final int index;
  final TextEditingController firstNameController;
  final TextEditingController lastNameController;
  final TextEditingController birthdayController;
  final TextEditingController citizenshipController;
  final TextEditingController interPassNumberController;
  final TextEditingController interPassNumberValidityController;
  final String? firstNameError;
  final String? lastNameError;
  final String? birthdayError;
  final String? citizenshipError;
  final String? interPassNumberError;
  final String? interPassNumberValidityError;
  const AddTouristInfo({
    Key? key,
    required this.index,
    required this.firstNameController,
    required this.lastNameController,
    required this.birthdayController,
    required this.citizenshipController,
    required this.interPassNumberController,
    required this.interPassNumberValidityController,
    required this.firstNameError,
    required this.lastNameError,
    required this.birthdayError,
    required this.citizenshipError,
    required this.interPassNumberError,
    required this.interPassNumberValidityError,
  }) : super(key: key);

  @override
  State<AddTouristInfo> createState() => _AddTouristInfoState();
}

class _AddTouristInfoState extends State<AddTouristInfo> {
  bool isExpanded = true;

  void onExpansionChanged(bool value) {
    setState(() {
      isExpanded = value;
    });
  }

  @override
  void initState() {
    isExpanded = true;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DisplayCard(
      content: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8),
        child: ListTileTheme(
          contentPadding: EdgeInsets.zero,
          child: ExpansionTile(
            onExpansionChanged: (value) => onExpansionChanged(value),
            trailing: ColoredWidget(
              widgetsColor: AppPallete.blue,
              backgroundColor: AppPallete.blueOpacity,
              suffixIcon: isExpanded ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down,
              padding: const EdgeInsets.all(4),
            ),
            initiallyExpanded: true,
            shape: InputBorder.none,
            title: BodyLargeText(text: 'Tурист ${widget.index + 1}'),
            children: [
              CustomTextField(
                hintText: CoreText.firstName,
                controller: widget.firstNameController,
                errorText: widget.firstNameError,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: CustomTextField(
                  hintText: CoreText.lastName,
                  controller: widget.lastNameController,
                  errorText: widget.lastNameError,
                ),
              ),
              CustomTextField(
                hintText: CoreText.birthday,
                controller: widget.birthdayController,
                errorText: widget.birthdayError,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: CustomTextField(
                  hintText: CoreText.citizenship,
                  controller: widget.citizenshipController,
                  errorText: widget.citizenshipError,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8),
                child: CustomTextField(
                  hintText: CoreText.interPassNumber,
                  controller: widget.interPassNumberController,
                  errorText: widget.interPassNumberError,
                ),
              ),
              CustomTextField(
                hintText: CoreText.interPassNumberValidity,
                controller: widget.interPassNumberValidityController,
                errorText: widget.interPassNumberValidityError,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
