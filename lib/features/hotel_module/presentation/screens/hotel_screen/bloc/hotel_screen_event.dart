abstract class HotelScreenEvent {
  const HotelScreenEvent();
}

class HotelScreenInitialEvent extends HotelScreenEvent {}

class NavigateToRoomScreenEvent extends HotelScreenEvent {}

class SlideImageEvent extends HotelScreenEvent {
  final int currentIndex;
  SlideImageEvent({
    required this.currentIndex,
  });
}
