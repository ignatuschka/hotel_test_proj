import 'package:built_value/built_value.dart';
import 'package:hotel_test_project/features/hotel_module/domain/entity/hotel_entity.dart';

part 'hotel_screen_state.g.dart';

abstract class HotelScreenState implements Built<HotelScreenState, HotelScreenStateBuilder> {
  bool get isLoading;
  int get currentIndex;
  HotelEntity? get hotel;

  HotelScreenState._();

  factory HotelScreenState([void Function(HotelScreenStateBuilder)? updates]) = _$HotelScreenState;

  factory HotelScreenState.initial() => HotelScreenState((b) => b
    ..isLoading = true
    ..currentIndex = 0
    ..hotel = null);

  HotelScreenState setIsLoading(bool isLoading) => rebuild((b) => (b)..isLoading = isLoading);

  HotelScreenState setCurrentIndex(int currentIndex) => rebuild((b) => (b)..currentIndex = currentIndex);

  HotelScreenState setHotel(HotelEntity hotel) => rebuild((b) => (b)..hotel = hotel);
}
