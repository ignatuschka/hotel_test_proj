import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:hotel_test_project/core/base_usecase.dart';
import 'package:hotel_test_project/features/hotel_module/domain/usecase/get_hotel_data_usecase.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/hotel_screen/bloc/hotel_screen_event.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/hotel_screen/bloc/hotel_screen_state.dart';
import 'package:hotel_test_project/navigation/app_navigator.dart';

class HotelScreenBloc extends Bloc<HotelScreenEvent, HotelScreenState> {
  final GetHotelDataUsecase getHotelDataUsecase;
  final CoreNavigator navigator;
  HotelScreenBloc({required this.getHotelDataUsecase, required this.navigator}) : super(HotelScreenState.initial()) {
    on<HotelScreenInitialEvent>(_initial);
    on<SlideImageEvent>(_slideImageEvent);
    on<NavigateToRoomScreenEvent>(_navigateToRoomScreen);
    add(HotelScreenInitialEvent());
  }

  Future<void> _initial(HotelScreenInitialEvent event, Emitter<HotelScreenState> emit) async {
    emit(state.setIsLoading(true));
    await processUseCase(() async => await getHotelDataUsecase(EmptyParams()), onSucess: (result) async {
      emit(state.setHotel(result));
      emit(state.setIsLoading(false));
    });
  }

  void _slideImageEvent(SlideImageEvent event, Emitter<HotelScreenState> emit) {
    emit(state.setCurrentIndex(event.currentIndex));
  }

  void _navigateToRoomScreen(NavigateToRoomScreenEvent event, Emitter<HotelScreenState> emit) {
    if (!state.isLoading) {
      navigator.navigateToHotelRoomScreen(hotelName: state.hotel?.name ?? '');
    }
  }
}
