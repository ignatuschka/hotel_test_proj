import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hotel_test_project/core/globals.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/hotel_screen/bloc/hotel_screen_bloc.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/hotel_screen/bloc/hotel_screen_event.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/hotel_screen/bloc/hotel_screen_state.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/colored_text.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/display_card.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/hotel_data_buttons.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/image_carousel.dart';
import 'package:hotel_test_project/resources/core_text.dart';
import 'package:hotel_test_project/widgets/body_large_text.dart';
import 'package:hotel_test_project/widgets/body_medium_secondary_text.dart';
import 'package:hotel_test_project/widgets/body_small_text.dart';
import 'package:hotel_test_project/widgets/custom_button.dart';
import 'package:hotel_test_project/widgets/app_bar_text.dart';
import 'package:hotel_test_project/widgets/custom_loading_indicator.dart';
import 'package:hotel_test_project/widgets/display_large_text.dart';

class HotelScreen extends StatelessWidget {
  const HotelScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const AppBarText(text: CoreText.hotel)),
      body: BlocBuilder<HotelScreenBloc, HotelScreenState>(
        builder: (context, state) {
          if (!state.isLoading) {
            return SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    child: DisplayCard(
                      content: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                              padding: const EdgeInsets.symmetric(vertical: 16),
                              child: ImageCarousel(
                                pagesCount: state.hotel?.imageUrls?.length ?? 0,
                                currentIndex: state.currentIndex,
                                urls: state.hotel?.imageUrls ?? [],
                                onPageChanged: (value) {
                                  context.read<HotelScreenBloc>().add(SlideImageEvent(currentIndex: value));
                                },
                              )),
                          ColoredWidget(
                            text: '${state.hotel?.rating} ${state.hotel?.ratingName}',
                            widgetsColor: AppPallete.orange,
                            backgroundColor: AppPallete.orangeOpacity,
                            prefixIcon: Icons.star,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8),
                            child: BodyLargeText(text: state.hotel?.name ?? ''),
                          ),
                          BodySmallText(
                            text: state.hotel?.adress ?? '',
                            color: AppPallete.blue,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                DisplayLargeText(
                                  text:
                                      'от ${formatter.format(state.hotel?.minimalPrice ?? 0).replaceAll(',', ' ')} \u20BD',
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8, bottom: 3),
                                  child: BodyMediumSecondaryText(
                                    text: state.hotel?.priceForIt?.toLowerCase() ?? '',
                                    color: AppPallete.greyText,
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 12),
                    child: DisplayCard(
                      content: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Padding(
                            padding: EdgeInsets.only(top: 16, bottom: 12),
                            child: BodyLargeText(text: CoreText.aboutHotel),
                          ),
                          Wrap(
                            children: List<Widget>.generate(
                              state.hotel?.aboutTheHotel?.peculiarities?.length ?? 0,
                              (index) => Padding(
                                padding: const EdgeInsets.all(4),
                                child: ColoredWidget(
                                  text: '${state.hotel?.aboutTheHotel?.peculiarities?[index]}',
                                  widgetsColor: AppPallete.greyText,
                                  backgroundColor: AppPallete.greyTextBackground,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8),
                            child: BodyMediumSecondaryText(text: state.hotel?.aboutTheHotel?.description ?? ''),
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(vertical: 16),
                            child: HotelDataButtons(assetPaths: [
                              'assets/emoji-happy.svg',
                              'assets/tick-square.svg',
                              'assets/close-square.svg'
                            ], titles: [
                              CoreText.facilities,
                              CoreText.included,
                              CoreText.notIncluded
                            ], subtitles: [
                              CoreText.mostNecessary,
                              CoreText.mostNecessary,
                              CoreText.mostNecessary
                            ], icons: [
                              Icons.keyboard_arrow_right,
                              Icons.keyboard_arrow_right,
                              Icons.keyboard_arrow_right,
                            ]),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else {
            return const CustomLoadingIndicator(color: AppPallete.blue);
          }
        },
      ),
      bottomNavigationBar: Container(
        decoration:
            const BoxDecoration(border: Border(top: BorderSide(color: AppPallete.greyStroke)), color: AppPallete.white),
        child: Padding(
          padding: EdgeInsets.fromLTRB(16, 12, 16, (MediaQuery.of(context).viewInsets.bottom == 0) ? 16 : 0),
          child: CustomButton(
            onTap: () {
              context.read<HotelScreenBloc>().add(NavigateToRoomScreenEvent());
            },
            text: CoreText.toRoom,
          ),
        ),
      ),
    );
  }
}
