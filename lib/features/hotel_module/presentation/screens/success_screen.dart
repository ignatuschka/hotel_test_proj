import 'package:flutter/material.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';
import 'package:hotel_test_project/navigation/app_navigator.dart';
import 'package:hotel_test_project/resources/core_text.dart';
import 'package:hotel_test_project/widgets/app_bar_text.dart';
import 'package:hotel_test_project/widgets/body_large_text.dart';
import 'package:hotel_test_project/widgets/body_medium_text.dart';
import 'package:hotel_test_project/widgets/custom_button.dart';

class SuccessPage extends StatelessWidget {
  const SuccessPage({super.key});

  @override
  Widget build(BuildContext context) {
    final navigator = CoreNavigatorImpl();
    return Scaffold(
      backgroundColor: AppPallete.white,
      appBar: AppBar(
        title: const AppBarText(text: CoreText.paidOrder),
        leading: IconButton(
            onPressed: () {
              navigator.navigateBack();
            },
            icon: const Padding(
              padding: EdgeInsets.all(6),
              child: Icon(Icons.keyboard_arrow_left),
            )),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: AppPallete.greyBackground,
              ),
              child: Padding(
                padding: const EdgeInsets.all(25),
                child: Image.asset(
                  'assets/Party_Popper.png',
                  width: 44,
                  height: 44,
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(top: 32, bottom: 20),
              child: BodyLargeText(text: CoreText.orderProcessed),
            ),
            const BodyMediumText(
              text: CoreText.successOrderInfo,
              color: AppPallete.greyText,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        decoration:
            const BoxDecoration(border: Border(top: BorderSide(color: AppPallete.greyStroke)), color: AppPallete.white),
        child: Padding(
          padding: EdgeInsets.fromLTRB(16, 12, 16, (MediaQuery.of(context).viewInsets.bottom == 0) ? 16 : 0),
          child: CustomButton(
            onTap: () {
              navigator.navigateToRoot();
            },
            text: CoreText.superText,
          ),
        ),
      ),
    );
  }
}
