abstract class BookingScreenEvent {
  const BookingScreenEvent();
}

class BookingScreenInitialEvent extends BookingScreenEvent {}

class NavigateToSuccessScreenEvent extends BookingScreenEvent {
  final String phoneNumber;
  final String email;
  final List<String> firstNames;
  final List<String> lastNames;
  final List<String> birthdays;
  final List<String> citizenships;
  final List<String> interPassNumbers;
  final List<String> interPassNumberValidities;
  NavigateToSuccessScreenEvent({
    required this.phoneNumber,
    required this.email,
    required this.firstNames,
    required this.lastNames,
    required this.birthdays,
    required this.citizenships,
    required this.interPassNumbers,
    required this.interPassNumberValidities,
  });
}

class AddTouristEvent extends BookingScreenEvent {}
