import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hotel_test_project/core/base_usecase.dart';
import 'package:hotel_test_project/features/hotel_module/domain/usecase/get_booking_data_usecase.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/booking_screen/bloc/booking_screen_event.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/booking_screen/bloc/booking_screen_state.dart';
import 'package:hotel_test_project/navigation/app_navigator.dart';
import 'package:hotel_test_project/resources/core_text.dart';

class BookingScreenBloc extends Bloc<BookingScreenEvent, BookingScreenState> {
  final GetBookingDataUsecase getBookingDataUsecase;
  final CoreNavigator navigator;
  BookingScreenBloc({required this.getBookingDataUsecase, required this.navigator})
      : super(BookingScreenState.initial()) {
    on<BookingScreenInitialEvent>(_initial);
    on<AddTouristEvent>(_addTouristEvent);
    on<NavigateToSuccessScreenEvent>(_navigateToSuccessScreenEvent);
    add(BookingScreenInitialEvent());
  }

  Future<void> _initial(BookingScreenInitialEvent event, Emitter<BookingScreenState> emit) async {
    emit(state.setIsLoading(true));
    await processUseCase(() async => await getBookingDataUsecase(EmptyParams()), onSucess: (result) async {
      emit(state.setBookingData(result));
      emit(state.setIsLoading(false));
    });
  }

  void _addTouristEvent(AddTouristEvent event, Emitter<BookingScreenState> emit) {
    emit(state.setTouristCount(state.touristCount + 1));
  }

  void _navigateToSuccessScreenEvent(NavigateToSuccessScreenEvent event, Emitter<BookingScreenState> emit) {
    if (event.phoneNumber.length != 10) {
      emit(state.setPhoneNumberError(CoreText.fillCell));
    } else {
      emit(state.setPhoneNumberError(null));
    }
    if (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.~!$%^&*_=+}{'?-]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(event.email)) {
      emit(state.setEmailError(CoreText.fillCell));
    } else {
      emit(state.setEmailError(null));
    }
    emit(state.setFirstNameErrorIndexes([]));
    emit(state.setLastNameErrorIndexes([]));
    emit(state.setBirthdayErrorIndexes([]));
    emit(state.setCitizenshipErrorIndexes([]));
    emit(state.setInterPassNumberErrorIndexes([]));
    emit(state.setInterPassNumberValidityErrorIndexes([]));
    for (var i = 0; i < state.touristCount; i++) {
      if (event.firstNames[i].isEmpty) {
        emit(state.setFirstNameErrorIndexes(state.firstNameErrorIndexes + [i]));
      }
      if (event.lastNames[i].isEmpty) {
        emit(state.setLastNameErrorIndexes(state.lastNameErrorIndexes + [i]));
      }
      if (event.birthdays[i].isEmpty) {
        emit(state.setBirthdayErrorIndexes(state.birthdayErrorIndexes + [i]));
      }
      if (event.citizenships[i].isEmpty) {
        emit(state.setCitizenshipErrorIndexes(state.citizenshipErrorIndexes + [i]));
      }
      if (event.interPassNumbers[i].isEmpty) {
        emit(state.setInterPassNumberErrorIndexes(state.interPassNumberErrorIndexes + [i]));
      }
      if (event.interPassNumberValidities[i].isEmpty) {
        emit(state.setInterPassNumberValidityErrorIndexes(state.interPassNumberValidityErrorIndexes + [i]));
      }
    }
    if (state.phoneNumberError == null &&
        state.emailError == null &&
        state.firstNameErrorIndexes.isEmpty &&
        state.lastNameErrorIndexes.isEmpty &&
        state.birthdayErrorIndexes.isEmpty &&
        state.citizenshipErrorIndexes.isEmpty &&
        state.interPassNumberErrorIndexes.isEmpty &&
        state.interPassNumberValidityErrorIndexes.isEmpty) {
          navigator.navigateToSuccessScreen();
        }
  }
}
