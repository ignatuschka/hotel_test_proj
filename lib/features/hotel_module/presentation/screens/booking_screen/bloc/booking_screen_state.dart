import 'package:built_value/built_value.dart';
import 'package:hotel_test_project/features/hotel_module/domain/entity/booking_entity.dart';

part 'booking_screen_state.g.dart';

abstract class BookingScreenState implements Built<BookingScreenState, BookingScreenStateBuilder> {
  bool get isLoading;
  BookingEntity? get bookingData;
  int get touristCount;
  String? get phoneNumberError;
  String? get emailError;
  List<int> get firstNameErrorIndexes;
  List<int> get lastNameErrorIndexes;
  List<int> get birthdayErrorIndexes;
  List<int> get citizenshipErrorIndexes;
  List<int> get interPassNumberErrorIndexes;
  List<int> get interPassNumberValidityErrorIndexes;
  bool get wasError;

  BookingScreenState._();

  factory BookingScreenState([void Function(BookingScreenStateBuilder)? updates]) = _$BookingScreenState;

  factory BookingScreenState.initial() => BookingScreenState((b) => b
    ..isLoading = true
    ..bookingData = null
    ..touristCount = 1
    ..phoneNumberError = null
    ..emailError = null
    ..firstNameErrorIndexes = []
    ..lastNameErrorIndexes = []
    ..birthdayErrorIndexes = []
    ..citizenshipErrorIndexes = []
    ..interPassNumberErrorIndexes = []
    ..interPassNumberValidityErrorIndexes = []
    ..wasError = false);

  BookingScreenState setIsLoading(bool isLoading) => rebuild((b) => (b)..isLoading = isLoading);

  BookingScreenState setBookingData(BookingEntity bookingData) => rebuild((b) => (b)..bookingData = bookingData);

  BookingScreenState setTouristCount(int touristCount) => rebuild((b) => (b)..touristCount = touristCount);

  BookingScreenState setPhoneNumberError(String? phoneNumberError) => rebuild((b) => (b)..phoneNumberError = phoneNumberError);

  BookingScreenState setEmailError(String? emailError) => rebuild((b) => (b)..emailError = emailError);

  BookingScreenState setFirstNameErrorIndexes(List<int> firstNameErrorIndexes) =>
      rebuild((b) => (b)..firstNameErrorIndexes = firstNameErrorIndexes);

  BookingScreenState setLastNameErrorIndexes(List<int> lastNameErrorIndexes) =>
      rebuild((b) => (b)..lastNameErrorIndexes = lastNameErrorIndexes);

  BookingScreenState setInterPassNumberErrorIndexes(List<int> interPassNumberErrorIndexes) =>
      rebuild((b) => (b)..interPassNumberErrorIndexes = interPassNumberErrorIndexes);

  BookingScreenState setInterPassNumberValidityErrorIndexes(List<int> interPassNumberValidityErrorIndexes) =>
      rebuild((b) => (b)..interPassNumberValidityErrorIndexes = interPassNumberValidityErrorIndexes);

  BookingScreenState setBirthdayErrorIndexes(List<int> birthdayErrorIndexes) =>
      rebuild((b) => (b)..birthdayErrorIndexes = birthdayErrorIndexes);

  BookingScreenState setCitizenshipErrorIndexes(List<int> citizenshipErrorIndexes) =>
      rebuild((b) => (b)..citizenshipErrorIndexes = citizenshipErrorIndexes);

  BookingScreenState setWasError(bool wasError) => rebuild((b) => (b)..wasError = wasError);
}
