import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hotel_test_project/core/globals.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/booking_screen/bloc/booking_screen_bloc.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/booking_screen/bloc/booking_screen_event.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/booking_screen/bloc/booking_screen_state.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/add_tourist_info.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/booking_info.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/buyer_info.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/colored_text.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/custom_mask.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/display_card.dart';
import 'package:hotel_test_project/navigation/app_navigator.dart';
import 'package:hotel_test_project/resources/core_text.dart';
import 'package:hotel_test_project/widgets/app_bar_text.dart';
import 'package:hotel_test_project/widgets/body_large_text.dart';
import 'package:hotel_test_project/widgets/body_medium_text.dart';
import 'package:hotel_test_project/widgets/body_small_text.dart';
import 'package:hotel_test_project/widgets/custom_button.dart';
import 'package:hotel_test_project/widgets/custom_loading_indicator.dart';

class BookingScreen extends StatelessWidget {
  const BookingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final navigator = CoreNavigatorImpl();
    final customMask = CustomMask();
    final emailController = TextEditingController();
    final List<TextEditingController> firstNameControllers = [];
    final List<TextEditingController> lastNameControllers = [];
    final List<TextEditingController> birthdayControllers = [];
    final List<TextEditingController> citizenshipControllers = [];
    final List<TextEditingController> interPassNumberControllers = [];
    final List<TextEditingController> interPassNumberValidityControllers = [];
    return BlocBuilder<BookingScreenBloc, BookingScreenState>(
      builder: (context, state) {
        if (!state.isLoading) {
          return Scaffold(
            appBar: AppBar(
              title: const AppBarText(text: CoreText.booking),
              leading: IconButton(
                  onPressed: () {
                    navigator.navigateBack();
                  },
                  icon: const Padding(
                    padding: EdgeInsets.all(6),
                    child: Icon(Icons.keyboard_arrow_left),
                  )),
            ),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    child: DisplayCard(
                      content: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ColoredWidget(
                              text: '${state.bookingData?.horating} ${state.bookingData?.ratingName}',
                              widgetsColor: AppPallete.orange,
                              backgroundColor: AppPallete.orangeOpacity,
                              prefixIcon: Icons.star,
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8),
                              child: BodyLargeText(text: state.bookingData?.hotelName ?? ''),
                            ),
                            BodySmallText(
                              text: state.bookingData?.hotelAdress ?? '',
                              color: AppPallete.blue,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  BookingInfo(
                    departure: state.bookingData?.departure ?? '',
                    arrivalCountry: state.bookingData?.arrivalCountry ?? '',
                    dates: '${state.bookingData?.tourDateStart ?? ''} - ${state.bookingData?.tourDateStop ?? ''}',
                    numberOfNights: '${state.bookingData?.numberOfNights ?? 0} ночей',
                    hotelName: state.bookingData?.hotelName ?? '',
                    room: state.bookingData?.room ?? '',
                    nutrition: state.bookingData?.nutrition ?? '',
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8),
                    child: BuyerInfo(
                      customMask: customMask,
                      emailController: emailController,
                      customMaskError: state.phoneNumberError,
                      emailError: state.emailError,
                    ),
                  ),
                  ListView.builder(
                    itemCount: state.touristCount,
                    physics: const NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      firstNameControllers.add(TextEditingController());
                      lastNameControllers.add(TextEditingController());
                      birthdayControllers.add(TextEditingController());
                      citizenshipControllers.add(TextEditingController());
                      interPassNumberControllers.add(TextEditingController());
                      interPassNumberValidityControllers.add(TextEditingController());
                      return Padding(
                        padding: const EdgeInsets.only(top: 8),
                        child: AddTouristInfo(
                          index: index,
                          firstNameController: firstNameControllers[index],
                          lastNameController: lastNameControllers[index],
                          birthdayController: birthdayControllers[index],
                          citizenshipController: citizenshipControllers[index],
                          interPassNumberController: interPassNumberControllers[index],
                          interPassNumberValidityController: interPassNumberValidityControllers[index],
                          firstNameError: state.firstNameErrorIndexes.contains(index) ? CoreText.fillCell : null,
                          lastNameError: state.lastNameErrorIndexes.contains(index) ? CoreText.fillCell : null,
                          birthdayError: state.birthdayErrorIndexes.contains(index) ? CoreText.fillCell : null,
                          citizenshipError: state.citizenshipErrorIndexes.contains(index) ? CoreText.fillCell : null,
                          interPassNumberError:
                              state.interPassNumberErrorIndexes.contains(index) ? CoreText.fillCell : null,
                          interPassNumberValidityError:
                              state.interPassNumberValidityErrorIndexes.contains(index) ? CoreText.fillCell : null,
                        ),
                      );
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    child: DisplayCard(
                      content: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16),
                          child: Row(
                            children: [
                              const BodyLargeText(text: CoreText.addTourist),
                              const Spacer(),
                              GestureDetector(
                                onTap: () {
                                  context.read<BookingScreenBloc>().add(AddTouristEvent());
                                },
                                child: const ColoredWidget(
                                  widgetsColor: AppPallete.white,
                                  backgroundColor: AppPallete.blue,
                                  suffixIcon: Icons.add,
                                  padding: EdgeInsets.all(4),
                                ),
                              ),
                            ],
                          )),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: DisplayCard(
                        content: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              const BodyMediumText(
                                text: CoreText.tour,
                                color: AppPallete.greyText,
                              ),
                              const Spacer(),
                              SizedBox(
                                child: BodyMediumText(
                                  text:
                                      '${formatter.format(state.bookingData?.tourPrice ?? 0).replaceAll(',', ' ')} \u20BD',
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16),
                            child: Row(
                              children: [
                                const BodyMediumText(
                                  text: CoreText.fuelSurcharge,
                                  color: AppPallete.greyText,
                                ),
                                const Spacer(),
                                SizedBox(
                                  child: BodyMediumText(
                                    text:
                                        '${formatter.format(state.bookingData?.fuelCharge).replaceAll(',', ' ')} \u20BD',
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 16),
                            child: Row(
                              children: [
                                const BodyMediumText(
                                  text: CoreText.serviceFee,
                                  color: AppPallete.greyText,
                                ),
                                const Spacer(),
                                SizedBox(
                                  child: BodyMediumText(
                                    text:
                                        '${formatter.format(state.bookingData?.serviceCharge).replaceAll(',', ' ')} \u20BD',
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            children: [
                              const BodyMediumText(
                                text: CoreText.toPay,
                                color: AppPallete.greyText,
                              ),
                              const Spacer(),
                              SizedBox(
                                child: BodyMediumText(
                                  text:
                                      '${formatter.format((state.bookingData?.tourPrice ?? 0) + (state.bookingData?.serviceCharge ?? 0) + (state.bookingData?.fuelCharge ?? 0)).replaceAll(',', ' ')}  \u20BD',
                                  color: AppPallete.blue,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    )),
                  ),
                ],
              ),
            ),
            bottomNavigationBar: Container(
              decoration: const BoxDecoration(
                  border: Border(top: BorderSide(color: AppPallete.greyStroke)), color: AppPallete.white),
              child: Padding(
                padding: EdgeInsets.fromLTRB(16, 12, 16, (MediaQuery.of(context).viewInsets.bottom == 0) ? 16 : 0),
                child: CustomButton(
                  onTap: () {
                    context.read<BookingScreenBloc>().add(NavigateToSuccessScreenEvent(
                          phoneNumber: customMask.getClearPhone(),
                          email: emailController.text,
                          firstNames: firstNameControllers.map((e) => e.text).toList(),
                          lastNames: lastNameControllers.map((e) => e.text).toList(),
                          birthdays: birthdayControllers.map((e) => e.text).toList(),
                          citizenships: citizenshipControllers.map((e) => e.text).toList(),
                          interPassNumbers: interPassNumberControllers.map((e) => e.text).toList(),
                          interPassNumberValidities: interPassNumberValidityControllers.map((e) => e.text).toList(),
                        ));
                  },
                  text:
                      'Оплатить ${formatter.format((state.bookingData?.tourPrice ?? 0) + (state.bookingData?.serviceCharge ?? 0) + (state.bookingData?.fuelCharge ?? 0)).replaceAll(',', ' ')}  \u20BD',
                ),
              ),
            ),
          );
        } else {
          return const CustomLoadingIndicator(color: AppPallete.blue);
        }
      },
    );
  }
}
