import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hotel_test_project/core/base_usecase.dart';
import 'package:hotel_test_project/features/hotel_module/domain/usecase/get_hotel_room_usecase.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/hotel_room_screen/bloc/hotel_room_screen_event.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/hotel_room_screen/bloc/hotel_room_screen_state.dart';
import 'package:hotel_test_project/navigation/app_navigator.dart';

class HotelRoomScreenBloc extends Bloc<HotelRoomScreenEvent, HotelRoomScreenState> {
  final GetHotelRoomsUsecase getHotelRoomsUsecase;
  final CoreNavigator navigator;
  HotelRoomScreenBloc({required this.getHotelRoomsUsecase, required this.navigator})
      : super(HotelRoomScreenState.initial()) {
    on<HotelRoomScreenInitialEvent>(_initial);
    on<NavigateToBookingScreen>(_navigateToBookingScreen);
    add(HotelRoomScreenInitialEvent());
  }

  Future<void> _initial(HotelRoomScreenInitialEvent event, Emitter<HotelRoomScreenState> emit) async {
    emit(state.setIsLoading(true));
    await processUseCase(() async => await getHotelRoomsUsecase(EmptyParams()), onSucess: (result) async {
      emit(state.setListRoom(result));
      emit(state.setIsLoading(false));
    });
  }

  void _navigateToBookingScreen(NavigateToBookingScreen event, Emitter<HotelRoomScreenState> emit) {
    if (!state.isLoading) {
      navigator.navigateToBookingScreen();
    }
  }
}
