abstract class HotelRoomScreenEvent {
  const HotelRoomScreenEvent();
}

class HotelRoomScreenInitialEvent extends HotelRoomScreenEvent {}

class NavigateToBookingScreen extends HotelRoomScreenEvent {}