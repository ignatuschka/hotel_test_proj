import 'package:built_value/built_value.dart';
import 'package:hotel_test_project/features/hotel_module/domain/entity/list_room_entity.dart';

part 'hotel_room_screen_state.g.dart';

abstract class HotelRoomScreenState implements Built<HotelRoomScreenState, HotelRoomScreenStateBuilder> {
  bool get isLoading;
  ListRoomEntity? get listRoom; 

  HotelRoomScreenState._();

  factory HotelRoomScreenState([void Function(HotelRoomScreenStateBuilder)? updates]) = _$HotelRoomScreenState;

  factory HotelRoomScreenState.initial() => HotelRoomScreenState((b) => b
    ..isLoading = true
    ..listRoom = null);

  HotelRoomScreenState setIsLoading(bool isLoading) => rebuild((b) => (b)..isLoading = isLoading);

  HotelRoomScreenState setListRoom(ListRoomEntity listRoom) => rebuild((b) => (b)..listRoom = listRoom);
}
