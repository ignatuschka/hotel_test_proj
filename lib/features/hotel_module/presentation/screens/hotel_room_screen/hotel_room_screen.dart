import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/hotel_room_screen/bloc/hotel_room_screen_bloc.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/hotel_room_screen/bloc/hotel_room_screen_event.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/hotel_room_screen/bloc/hotel_room_screen_state.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/widgets/room_card.dart';
import 'package:hotel_test_project/navigation/app_navigator.dart';
import 'package:hotel_test_project/widgets/app_bar_text.dart';
import 'package:hotel_test_project/widgets/custom_loading_indicator.dart';

class HotelRoomsScreen extends StatelessWidget {
  final String hotelName;
  const HotelRoomsScreen({
    Key? key,
    required this.hotelName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final navigator = CoreNavigatorImpl();
    return Scaffold(
      appBar: AppBar(
        title: AppBarText(text: hotelName),
        leading: IconButton(
            onPressed: () {
              navigator.navigateBack();
            },
            icon: const Padding(
              padding: EdgeInsets.all(6),
              child: Icon(Icons.keyboard_arrow_left),
            )),
      ),
      body: BlocBuilder<HotelRoomScreenBloc, HotelRoomScreenState>(
        builder: (context, state) {
          if (!state.isLoading) {
            return ListView.builder(
              itemCount: state.listRoom?.rooms?.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4),
                  child: RoomCard(
                    roomImageUrls: state.listRoom?.rooms?[index].imageUrls ?? [],
                    roomText: state.listRoom?.rooms?[index].name ?? '',
                    aboutRoom: state.listRoom?.rooms?[index].peculiarities ?? [],
                    roomCost: state.listRoom?.rooms?[index].price ?? 0,
                    onTap: () {
                      context.read<HotelRoomScreenBloc>().add(NavigateToBookingScreen());
                    },
                    costForWhat: state.listRoom?.rooms?[index].pricePer ?? '',
                  ),
                );
              },
            );
          } else {
            return const CustomLoadingIndicator(color: AppPallete.blue);
          }
        },
      ),
    );
  }
}
