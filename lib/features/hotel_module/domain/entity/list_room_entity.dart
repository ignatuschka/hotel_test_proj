import 'package:hotel_test_project/features/hotel_module/domain/entity/room_entity.dart';

class ListRoomEntity {
  final List<RoomEntity>? rooms;
  const ListRoomEntity({
    required this.rooms,
  });
}
