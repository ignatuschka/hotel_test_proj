class AboutHotelEntity {
  final String? description;
  final List<String>? peculiarities;
  const AboutHotelEntity({
    required this.description,
    required this.peculiarities,
  });
}
