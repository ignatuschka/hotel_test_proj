import 'package:hotel_test_project/features/hotel_module/domain/entity/booking_entity.dart';
import 'package:hotel_test_project/features/hotel_module/domain/entity/hotel_entity.dart';
import 'package:hotel_test_project/features/hotel_module/domain/entity/list_room_entity.dart';

abstract class HotelRepository {
  Future<HotelEntity> getHotelData();

  Future<ListRoomEntity> getHotelRooms();

  Future<BookingEntity> getBookingData();
}
