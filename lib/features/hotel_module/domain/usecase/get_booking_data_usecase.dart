import 'package:hotel_test_project/core/base_usecase.dart';
import 'package:hotel_test_project/features/hotel_module/domain/entity/booking_entity.dart';
import 'package:hotel_test_project/features/hotel_module/domain/repository/hotel_repository.dart';

class GetBookingDataUsecase extends BaseUsecase<BookingEntity, EmptyParams> {
  final HotelRepository repository;
  GetBookingDataUsecase({
    required this.repository,
  });

  @override
  Future<BookingEntity> call(EmptyParams params) async {
    return await repository.getBookingData();
  }
}
