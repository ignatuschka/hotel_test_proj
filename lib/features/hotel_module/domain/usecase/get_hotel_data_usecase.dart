import 'package:hotel_test_project/core/base_usecase.dart';
import 'package:hotel_test_project/features/hotel_module/domain/entity/hotel_entity.dart';
import 'package:hotel_test_project/features/hotel_module/domain/repository/hotel_repository.dart';

class GetHotelDataUsecase extends BaseUsecase<HotelEntity, EmptyParams> {
  final HotelRepository repository;
  GetHotelDataUsecase({
    required this.repository,
  });

  @override
  Future<HotelEntity> call(EmptyParams params) async {
    return await repository.getHotelData();
  }
}
