import 'package:hotel_test_project/core/base_usecase.dart';
import 'package:hotel_test_project/features/hotel_module/domain/entity/list_room_entity.dart';
import 'package:hotel_test_project/features/hotel_module/domain/repository/hotel_repository.dart';

class GetHotelRoomsUsecase extends BaseUsecase<ListRoomEntity, EmptyParams> {
  final HotelRepository repository;
  GetHotelRoomsUsecase({
    required this.repository,
  });

  @override
  Future<ListRoomEntity> call(EmptyParams params) async {
    return await repository.getHotelRooms();
  }
}
