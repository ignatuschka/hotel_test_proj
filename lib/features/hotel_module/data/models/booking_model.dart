import 'package:hotel_test_project/features/hotel_module/domain/entity/booking_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'booking_model.g.dart';

@JsonSerializable(createToJson: false)
class BookingModel {
  final int? id;
  @JsonKey(name: 'hotel_name')
  final String? hotelName;
  @JsonKey(name: 'hotel_adress')
  final String? hotelAdress;
  final int? horating;
  @JsonKey(name: 'rating_name')
  final String? ratingName;
  final String? departure;
  @JsonKey(name: 'arrival_country')
  final String? arrivalCountry;
  @JsonKey(name: 'tour_date_start')
  final String? tourDateStart;
  @JsonKey(name: 'tour_date_stop')
  final String? tourDateStop;
  @JsonKey(name: 'number_of_nights')
  final int? numberOfNights;
  final String? room;
  final String? nutrition;
  @JsonKey(name: 'tour_price')
  final int? tourPrice;
  @JsonKey(name: 'fuel_charge')
  final int? fuelCharge;
  @JsonKey(name: 'service_charge')
  final int? serviceCharge;
  const BookingModel({
    required this.id,
    required this.hotelName,
    required this.hotelAdress,
    required this.horating,
    required this.ratingName,
    required this.departure,
    required this.arrivalCountry,
    required this.tourDateStart,
    required this.tourDateStop,
    required this.numberOfNights,
    required this.room,
    required this.nutrition,
    required this.tourPrice,
    required this.fuelCharge,
    required this.serviceCharge,
  });

  factory BookingModel.fromJson(Map<String, dynamic> json) => _$BookingModelFromJson(json);

  BookingEntity toEntity() => BookingEntity(
        id: id,
        hotelName: hotelName,
        hotelAdress: hotelAdress,
        horating: horating,
        ratingName: ratingName,
        departure: departure,
        arrivalCountry: arrivalCountry,
        tourDateStart: tourDateStart,
        tourDateStop: tourDateStop,
        numberOfNights: numberOfNights,
        room: room,
        nutrition: nutrition,
        tourPrice: tourPrice,
        fuelCharge: fuelCharge,
        serviceCharge: serviceCharge,
      );
}
