import 'package:hotel_test_project/features/hotel_module/data/models/room_model.dart';
import 'package:hotel_test_project/features/hotel_module/domain/entity/list_room_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'list_room_model.g.dart';

@JsonSerializable(createToJson: false)
class ListRoomModel {
  final List<RoomModel>? rooms;
  const ListRoomModel({
    required this.rooms,
  });

  factory ListRoomModel.fromJson(Map<String, dynamic> json) => _$ListRoomModelFromJson(json);

  ListRoomEntity toEntity() => ListRoomEntity(
        rooms: rooms?.map((e) => e.toEntity()).toList(),
      );
}
