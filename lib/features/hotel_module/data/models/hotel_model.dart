import 'package:hotel_test_project/features/hotel_module/data/models/about_hotel_model.dart';
import 'package:hotel_test_project/features/hotel_module/domain/entity/hotel_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'hotel_model.g.dart';

@JsonSerializable(createToJson: false)
class HotelModel {
  final int? id;
  final String? name;
  final String? adress;
  @JsonKey(name: 'minimal_price')
  final int? minimalPrice;
  @JsonKey(name: 'price_for_it')
  final String? priceForIt;
  final int? rating;
  @JsonKey(name: 'rating_name')
  final String? ratingName;
  @JsonKey(name: 'image_urls')
  final List<String>? imageUrls;
  @JsonKey(name: 'about_the_hotel')
  final AboutHotelModel? aboutTheHotel;
  const HotelModel({
    required this.id,
    required this.name,
    required this.adress,
    required this.minimalPrice,
    required this.priceForIt,
    required this.rating,
    required this.ratingName,
    required this.imageUrls,
    required this.aboutTheHotel,
  });

  factory HotelModel.fromJson(Map<String, dynamic> json) => _$HotelModelFromJson(json);

  HotelEntity toEntity() => HotelEntity(
        id: id,
        name: name,
        adress: adress,
        minimalPrice: minimalPrice,
        ratingName: ratingName,
        priceForIt: priceForIt,
        rating: rating,
        imageUrls: imageUrls,
        aboutTheHotel: aboutTheHotel?.toEntity(),
      );
}
