import 'package:hotel_test_project/features/hotel_module/domain/entity/about_hotel_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'about_hotel_model.g.dart';

@JsonSerializable(createToJson: false)
class AboutHotelModel {
  final String? description;
  final List<String>? peculiarities;
  const AboutHotelModel({
    required this.description,
    required this.peculiarities,
  });

  factory AboutHotelModel.fromJson(Map<String, dynamic> json) => _$AboutHotelModelFromJson(json);

  AboutHotelEntity toEntity() => AboutHotelEntity(
        description: description,
        peculiarities: peculiarities,
      );
}
