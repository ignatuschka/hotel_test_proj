import 'package:hotel_test_project/features/hotel_module/domain/entity/room_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'room_model.g.dart';

@JsonSerializable(createToJson: false)
class RoomModel {
  final int? id;
  final String? name;
  final int? price;
  @JsonKey(name: 'price_per')
  final String? pricePer;
  final List<String>? peculiarities;
  @JsonKey(name: 'image_urls')
  final List<String>? imageUrls;
  RoomModel({
    required this.id,
    required this.name,
    required this.price,
    required this.pricePer,
    required this.peculiarities,
    required this.imageUrls,
  });

  factory RoomModel.fromJson(Map<String, dynamic> json) => _$RoomModelFromJson(json);

  RoomEntity toEntity() => RoomEntity(
        id: id,
        name: name,
        price: price,
        pricePer: pricePer,
        peculiarities: peculiarities,
        imageUrls: imageUrls,
      );
}
