import 'package:hotel_test_project/features/hotel_module/data/models/booking_model.dart';
import 'package:hotel_test_project/features/hotel_module/data/models/hotel_model.dart';
import 'package:hotel_test_project/features/hotel_module/data/models/list_room_model.dart';
import 'package:hotel_test_project/features/hotel_module/data/network/hotel_api.dart';

abstract class HotelRemoteDataSource {
  Future<HotelModel> getHotelData();

  Future<ListRoomModel> getHotelRooms();

  Future<BookingModel> getBookingData();
}

class HotelRemoteDataSourceImpl extends HotelRemoteDataSource {
  final HotelApi hotelApi;
  HotelRemoteDataSourceImpl({required this.hotelApi});

  @override
  Future<BookingModel> getBookingData() async => await hotelApi.getBookingData();

  @override
  Future<HotelModel> getHotelData() async => await hotelApi.getHotelData();

  @override
  Future<ListRoomModel> getHotelRooms() async => await hotelApi.getHotelRooms();
}
