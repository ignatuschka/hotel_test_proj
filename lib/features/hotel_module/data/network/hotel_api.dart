import 'package:dio/dio.dart';
import 'package:hotel_test_project/features/hotel_module/data/models/booking_model.dart';
import 'package:hotel_test_project/features/hotel_module/data/models/hotel_model.dart';
import 'package:hotel_test_project/features/hotel_module/data/models/list_room_model.dart';
import 'package:retrofit/retrofit.dart';

part 'hotel_api.g.dart';

@RestApi(baseUrl: 'https://run.mocky.io/v3')
abstract class HotelApi {
  factory HotelApi(Dio dio, {String baseUrl}) = _HotelApi;

  @GET('/d144777c-a67f-4e35-867a-cacc3b827473')
  Future<HotelModel> getHotelData();

  @GET('/8b532701-709e-4194-a41c-1a903af00195')
  Future<ListRoomModel> getHotelRooms();

  @GET('/63866c74-d593-432c-af8e-f279d1a8d2ff')
  Future<BookingModel> getBookingData();
}
