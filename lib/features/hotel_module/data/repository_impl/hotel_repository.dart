import 'package:hotel_test_project/features/hotel_module/data/data_source/hotel_remote_data_source.dart';
import 'package:hotel_test_project/features/hotel_module/domain/entity/booking_entity.dart';
import 'package:hotel_test_project/features/hotel_module/domain/entity/hotel_entity.dart';
import 'package:hotel_test_project/features/hotel_module/domain/entity/list_room_entity.dart';
import 'package:hotel_test_project/features/hotel_module/domain/repository/hotel_repository.dart';

class HotelRepositoryImpl extends HotelRepository {
  final HotelRemoteDataSource remoteDataSource;
  HotelRepositoryImpl({
    required this.remoteDataSource,
  });

  @override
  Future<BookingEntity> getBookingData() async {
    final result = await remoteDataSource.getBookingData();
    return result.toEntity();
  }

  @override
  Future<HotelEntity> getHotelData() async {
    final result = await remoteDataSource.getHotelData();
    return result.toEntity();
  }

  @override
  Future<ListRoomEntity> getHotelRooms() async {
    final result = await remoteDataSource.getHotelRooms();
    return result.toEntity();
  }
}
