import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:hotel_test_project/core/core_di_module.dart';
import 'package:hotel_test_project/core/globals.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';
import 'package:hotel_test_project/navigation/app_router.dart';

GetIt getIt = GetIt.I;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: AppPallete.transparent,
    systemNavigationBarColor: AppPallete.transparent,
    systemNavigationBarDividerColor: AppPallete.transparent,
  ));
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await CoreDIModule().updateInjections(getIt);
  await Future.delayed(const Duration(seconds: 2));
  runApp(MaterialApp.router(
    theme: ThemeData(
      useMaterial3: true,
      scaffoldBackgroundColor: AppPallete.greyBackground,
      appBarTheme: const AppBarTheme(
        color: AppPallete.white,
        iconTheme: IconThemeData(weight: 32),
        elevation: 0,
        centerTitle: true,
      ),
    ),
    scaffoldMessengerKey: scaffoldKey,
    routerDelegate: AutoRouterDelegate(getIt<AppRouter>()),
    routeInformationParser: getIt<AppRouter>().defaultRouteParser(),
  ));
}
