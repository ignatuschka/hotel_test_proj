import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';
import 'package:hotel_test_project/widgets/body_small_text.dart';

class CustomTextField extends StatelessWidget {
  final TextEditingController? controller;
  final String? errorText;
  final String hintText;
  final bool obscureText;
  final IconData? suffixIcon;
  final void Function(String)? onChanged;
  final void Function()? onSuffixIconPressed;
  final List<TextInputFormatter>? inputFormatters;
  const CustomTextField({
    Key? key,
    this.controller,
    this.errorText,
    required this.hintText,
    this.suffixIcon,
    this.obscureText = false,
    this.onChanged,
    this.onSuffixIconPressed,
    this.inputFormatters,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
            border: errorText != null ? Border.all(color: AppPallete.red) : null,
            borderRadius: BorderRadius.circular(10),
            color: AppPallete.greyBackground,
          ),
          child: Padding(
            padding: const EdgeInsets.only(left: 16, top: 10, bottom: 10),
            child: TextFormField(
              style: const TextStyle(
                fontWeight: FontWeight.w400,
                fontStyle: FontStyle.normal,
                fontSize: 16,
                height: 17.6 / 16,
                color: AppPallete.textfieldText,
                overflow: TextOverflow.fade,
              ),
              inputFormatters: inputFormatters,
              onTapOutside: (event) => FocusScope.of(context).unfocus(),
              controller: controller,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.zero,
                border: InputBorder.none,
                labelText: hintText,
                labelStyle: const TextStyle(
                  color: AppPallete.textfieldHint,
                  fontWeight: FontWeight.w400,
                  fontStyle: FontStyle.normal,
                  fontSize: 16,
                  height: 20.4 / 16,
                  overflow: TextOverflow.fade,
                ),
              ),
              onChanged: onChanged,
            ),
          ),
        ),
        //C:\Users\zxccx\Workspace\hotel_test_project
        if (errorText != null) ...[
          const SizedBox(height: 4),
          BodySmallText(
            text: errorText!,
            color: AppPallete.red,
          ),
        ]
      ],
    );
  }
}
