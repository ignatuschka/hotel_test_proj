import 'package:flutter/material.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';

class BodyMediumText extends StatelessWidget {
  final String text;
  final Color? color;
  final TextAlign? textAlign;

  const BodyMediumText({
    Key? key,
    required this.text,
    this.color,
    this.textAlign,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: textAlign,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
        fontSize: 16,
        height: 19.2 / 16,
        color: color ?? AppPallete.black,
        overflow: TextOverflow.fade,
      ),
    );
  }
}
