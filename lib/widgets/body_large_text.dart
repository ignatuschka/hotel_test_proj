import 'package:flutter/material.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';

class BodyLargeText extends StatelessWidget {
  final String text;
  final Color? color;

  const BodyLargeText({
    Key? key,
    required this.text,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
        fontSize: 22,
        height: 26.4 / 22,
        color: color ?? AppPallete.black,
        overflow: TextOverflow.fade,
      ),
    );
  }
}
