import 'package:flutter/material.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';
import 'package:hotel_test_project/widgets/button_text.dart';
import 'package:hotel_test_project/widgets/custom_loading_indicator.dart';

class CustomButton extends StatelessWidget {
  final void Function() onTap;
  final String text;
  final bool state;
  final double height;
  final double width;
  const CustomButton({
    Key? key,
    required this.onTap,
    required this.text,
    this.height = 48,
    this.width = double.infinity,
    this.state = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      borderRadius: BorderRadius.circular(16),
      child: Container(
        decoration: BoxDecoration(
          color: AppPallete.blue,
          borderRadius: BorderRadius.circular(16),
        ),
        height: height,
        width: width,
        child: Center(
          child: state
              ? const CustomLoadingIndicator()
              : ButtonText(
                  text: text,
                  color: AppPallete.white,
                ),
        ),
      ),
    );
  }
}
