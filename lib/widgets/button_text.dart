import 'package:flutter/material.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';

class ButtonText extends StatelessWidget {
  final String text;
  final Color? color;

  const ButtonText({
    Key? key,
    required this.text,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w500,
        fontStyle: FontStyle.normal,
        fontSize: 16,
        height: 17.6 / 16,
        color: color ?? AppPallete.black,
        overflow: TextOverflow.fade,
      ),
    );
  }
}
