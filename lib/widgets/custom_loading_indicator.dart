import 'package:flutter/material.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';

class CustomLoadingIndicator extends StatelessWidget {
  final double strokeWidth;
  final Color? color;

  const CustomLoadingIndicator({
    Key? key,
    this.strokeWidth = 4.0,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: AppPallete.greyBackground),
      width: double.infinity,
      height: double.infinity,
      child: Center(
        child: CircularProgressIndicator(
          color: color ?? AppPallete.white,
          strokeWidth: strokeWidth,
        ),
      ),
    );
  }
}
