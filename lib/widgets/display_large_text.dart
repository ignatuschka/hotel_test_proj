import 'package:flutter/material.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';

class DisplayLargeText extends StatelessWidget {
  final String text;
  final Color? color;

  const DisplayLargeText({
    Key? key,
    required this.text,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        fontWeight: FontWeight.w600,
        fontStyle: FontStyle.normal,
        fontSize: 30,
        height: 36 / 30,
        color: color ?? AppPallete.black,
        overflow: TextOverflow.fade,
      ),
    );
  }
}
