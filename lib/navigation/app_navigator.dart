import 'package:get_it/get_it.dart';
import 'package:hotel_test_project/navigation/app_router.dart';

GetIt getIt = GetIt.instance;

abstract class CoreNavigator {
  Future<void> navigateToHotelRoomScreen({required String hotelName});
  Future<void> navigateToBookingScreen();
  Future<void> navigateToSuccessScreen();
  void navigateToRoot();
  void navigateBack();
}

class CoreNavigatorImpl implements CoreNavigator {
  static final AppRouter _ar = getIt<AppRouter>();

  @override
  Future<void> navigateToHotelRoomScreen({required String hotelName}) async {
    _ar.push(HotelRoomRoute(hotelName: hotelName));
  }

  @override
  Future<void> navigateToBookingScreen() async {
    _ar.push(const BookingRoute());
  }

  @override
  Future<void> navigateToSuccessScreen() async {
    _ar.push(const SuccessRoute());
  }

  @override
  void navigateToRoot() {
    _ar.popUntilRoot();
  }

  @override
  void navigateBack() {
    _ar.pop();
  }
}
