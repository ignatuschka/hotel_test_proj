import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:hotel_test_project/features/hotel_module/di/hotel_di.dart';
import 'package:hotel_test_project/features/hotel_module/presentation/screens/success_screen.dart';

part 'app_router.gr.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: [
    AutoRoute(
      initial: true,
      page: HotelPage,
    ),
    AutoRoute(
      page: HotelRoomPage,
    ),
    AutoRoute(
      page: BookingPage,
    ),
    AutoRoute(
      page: SuccessPage,
    ),
  ],
)
class AppRouter extends _$AppRouter {}
