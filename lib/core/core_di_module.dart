import 'package:get_it/get_it.dart';
import 'package:hotel_test_project/features/hotel_module/di/hotel_di.dart';
import 'package:hotel_test_project/navigation/app_navigator.dart';
import 'package:hotel_test_project/navigation/app_router.dart';

class CoreDIModule {
  Future<void> updateInjections(GetIt instance) async {
    instance.registerSingleton<AppRouter>(AppRouter());
    instance.registerSingleton<CoreNavigator>(CoreNavigatorImpl());
    HotelDIModule().updateInjections(instance);
  }
}
