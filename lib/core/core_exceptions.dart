import 'package:dio/dio.dart';

abstract class CoreException {
  final String messageExseption;
  const CoreException(this.messageExseption);
}

abstract class CoreDioException extends DioError implements CoreException {
  @override
  final String messageExseption;
  CoreDioException(this.messageExseption, {required super.requestOptions});
}

class RequestCancelledException extends CoreDioException {
  RequestCancelledException({required super.requestOptions}) : super('Запрос был отменен. Попробуйте еще раз');
}

class ConnectTimeoutException extends CoreDioException {
  ConnectTimeoutException({required super.requestOptions}) : super('Ошибка соединения. Попробуйте еще раз');
}

class ReceiveTimeoutException extends CoreDioException {
  ReceiveTimeoutException({required super.requestOptions}) : super('Ошибка получения ответа. Попробуйте еще раз');
}

class SendTimeoutException extends CoreDioException {
  SendTimeoutException({required super.requestOptions}) : super('Ошибка отправки запроса. Попробуйте еще раз');
}

class UnknownServerException extends CoreDioException {
  final int? statusCode;
  UnknownServerException({required this.statusCode, required super.requestOptions})
      : super('Код сетевой ошибки: $statusCode. Попробуйте позднее');
}

class DefaultNetworkException extends CoreDioException {
  DefaultNetworkException({required super.requestOptions}) : super('Что-то не так. Проверьте интнрнет-соединение или Попробуйте позднее');
}
