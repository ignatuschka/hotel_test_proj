import 'package:flutter/material.dart';

class AppPallete {
  static const Color greyBackground = Color(0xFFF6F6F9);

  static const Color greyStroke = Color(0xFFE8E9EC);

  static const Color white = Color(0xFFFFFFFF);

  static const Color black = Color(0xFF000000);

  static const Color blue = Color(0xFF0D72FF);

  static const Color blueOpacity = Color(0x1A0D72FF);

  static const Color red = Color(0xD9EB5757);

  static const Color orange = Color(0xFFFFA800);

  static const Color orangeOpacity = Color(0x33FFC700);

  static const Color greyText = Color(0xFF828796);

  static const Color greyTextBackground = Color(0xFFFBFBFC);

  static const Color greyBorder = Color(0xFFE4E6EC);

  static const Color transparent = Colors.transparent;

  static const Color textfieldHint = Color(0xFFA9ABB7);

  static const Color textfieldText = Color(0xFF14142B);
}