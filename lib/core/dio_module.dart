import 'package:dio/dio.dart';
import 'package:hotel_test_project/core/core_interceptor.dart';

abstract class DioModule {
  static Dio provideDio() {
    Dio dio = Dio();
    BaseOptions options = BaseOptions(
      connectTimeout: 5000,
      sendTimeout: 3000,
    );
    dio = Dio(options);
    dio.interceptors.addAll([
      LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
        requestHeader: true,
      ),
      CoreInterceptor(),
    ]);

    return dio;
  }
}
