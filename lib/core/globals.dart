import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hotel_test_project/core/theme/app_pallete.dart';
import 'package:hotel_test_project/resources/core_text.dart';
import 'package:hotel_test_project/widgets/body_small_text.dart';
import 'package:intl/intl.dart';

final GlobalKey<ScaffoldMessengerState> scaffoldKey = GlobalKey<ScaffoldMessengerState>();

void showCustomSnackBar({
  required String event,
  String? label,
  Future<void> Function()? onTap,
}) async {
  await Future.microtask(
    () => scaffoldKey.currentState?.showSnackBar(
      SnackBar(
        shape: OutlineInputBorder(
          borderRadius: BorderRadius.circular(24),
          borderSide: const BorderSide(color:  AppPallete.red),
        ),
        actionOverflowThreshold: 1,
        elevation: 0,
        behavior: SnackBarBehavior.floating,
        margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 24),
        backgroundColor:  AppPallete.white,
        content: BodySmallText(
          text: event,
          color: AppPallete.red,
        ),
        action: SnackBarAction(
          textColor: AppPallete.red,
          label: label ?? CoreText.copy,
          onPressed: () async {
            if(onTap != null) {
              await onTap();
            } else {
              Clipboard.setData(ClipboardData(text: event));
            }
          },
        ),
      ),
    ),
  );
}

final formatter = NumberFormat('#,###');
