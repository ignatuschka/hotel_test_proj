import 'dart:async';

import 'package:hotel_test_project/core/core_exceptions.dart';
import 'package:hotel_test_project/core/globals.dart';

abstract class BaseUsecase<Type, Params> {
  Future<Type> call(Params params);
}

class EmptyParams {}

Future<void> processUseCase<Type>(
  Future<Type> Function() computation, {
  required Future<void> Function(Type result) onSucess,
  Future<void> Function(String exception)? onError,
}) async {
  try {
    final result = await computation();

    await onSucess(result);
  } on CoreDioException catch (e) {
    if (onError != null) {
      await onError(e.messageExseption);
    } else {
      showCustomSnackBar(event: e.messageExseption);
    }
  }
}
