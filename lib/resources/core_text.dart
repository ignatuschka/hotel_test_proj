abstract class CoreText {
  static const hotel = 'Отель';
  static const copy = 'Скопировать';
  static const toRoom = 'К выбору номера';
  static const aboutHotel = 'Об отеле';
  static const facilities = 'Удобства';
  static const mostNecessary  = 'Самое необходимое';
  static const included  = 'Что включено';
  static const notIncluded  = 'Что не включено';
  static const moreRoomDerailes  = 'Подробнее о номере';
  static const chooseRoom  = 'Выбрать номер';
  static const booking  = 'Бронирование';
  static const flightFrom  = 'Вылет из';
  static const countryCity  = 'Страна, город';
  static const nightCount  = 'Кол-во ночей';
  static const room  = 'Номер';
  static const meals  = 'Питание';
  static const buyerInfo  = 'Информация о покупателе';
  static const telNumber  = 'Номер телефона';
  static const email  = 'Почта';
  static const infoDescription = 'Эти данные никому не передаются. После оплаты мы вышлем чек на указанный вами номер и почту';
  static const firstName  = 'Имя';
  static const lastName  = 'Фамилия';
  static const birthday  = 'Дата рождения';
  static const citizenship  = 'Гражданство';
  static const interPassNumber  = 'Номер загранпаспорта';
  static const interPassNumberValidity  = 'Срок действия загранпаспорта';
  static const tour  = 'Тур';
  static const fuelSurcharge  = 'Топливный сбор';
  static const serviceFee  = 'Сервисный сбор';
  static const toPay  = 'К оплате';
  static const dates  = 'Даты';
  static const addTourist  = 'Добавить туриста';
  static const fillCell  = 'Заполните поле';
  static const orderProcessed  = 'Ваш заказ принят в работу';
  static const paidOrder  = 'Заказ оплачен';
  static const superText  = 'Супер!';
  static const successOrderInfo = 'Подтверждение заказа №104893 может\nзанять некоторое время (от 1 часа до суток).\nКак только мы получим ответ от\nтуроператора, вам на почту придет\nуведомление.';
}
